package muslim.hafidi.uts

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : AppCompatActivity () , View.OnClickListener {
    var fbAuth = FirebaseAuth.getInstance()

    override fun onClick(v: View?){
        when(v?.id){
            R.id.btnUpload ->{
                var intent = Intent(this,UploadActivity::class.java)
                startActivity(intent)
            }
            R.id.btnData ->{

                var intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        btnData.setOnClickListener(this)
        btnUpload.setOnClickListener(this)
        btnLogOff.setOnClickListener{
            fbAuth.signOut()
            this.finish()
        }
    }
}